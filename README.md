# Summa  

> Summa is an Open Source Software to manage your organization's activity

## Features

- Dashboard
- Clients
- Projects
- Products
- Invoices
- Expenses
- Bank Reconciliation
- Taxes
- Settings

## Installation

> TODO 
  
## RESTful API Response Codes  
 
> Inspired from Amazon RESTful API Response Codes
  
| HTTP Status Code          | Description                                                                                              |  
|---------------------------|----------------------------------------------------------------------------------------------------------|  
| 200 OK                    | Successful                                                                                               |  
| 201 Created               | Resource created                                                                                         |  
| 400 Bad Request           | Bad input parameter. Error message should indicate which one and why                                     |  
| 401 Unauthorized          | The client passed in the invalid Auth token                                                              |  
| 403 Forbidden             | The request was valid, but the server is refusing action                                                 |  
| 404 Not Found             | Resource not found                                                                                       |  
| 405 Method Not Allowed    | The resource does not support the specified HTTP verb                                                    |  
| 412 Precondition Failed   | Precondition failed                                                                                      | 
| 500 Internal Server Error | Servers are not working as expected. The request is probably valid but needs to be requested again later | 

## License

Summa is Open Source software released under the GNU General Public License v3.0 license.
