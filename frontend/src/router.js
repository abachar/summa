import Vue from "vue";
import Router from "vue-router";
import Dashboard from "./features/dashboard/Dashboard.vue";
import ListProducts from "./features/products/List.vue";
import CreateProduct from "./features/products/Create.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "dashboard",
      component: Dashboard,
      meta: { breadcrumb: [{ Dashboard: "" }] }
    },
    {
      path: "/products",
      name: "products",
      component: ListProducts,
      meta: { breadcrumb: [{ Dashboard: "dashboard" }, { Products: "" }] }
    },
    {
      path: "/products/create",
      name: "create-product",
      component: CreateProduct,
      meta: {
        breadcrumb: [
          { Dashboard: "dashboard" },
          { Products: "products" },
          { Create: "" }
        ]
      }
    }
  ]
});
