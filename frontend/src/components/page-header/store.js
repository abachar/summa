import store from "@/store";
import router from "@/router";

store.registerModule("page-header", {
  namespaced: true,
  state: {
    breadcrumb: []
  },
  mutations: {
    SET_BREADCRUMB(state, payload) {
      state.breadcrumb = payload.map(it => ({
        name: Object.keys(it)[0],
        path: Object.values(it)[0]
      }));
    }
  },
  actions: {}
});

router.afterEach(route => {
  store.commit("page-header/SET_BREADCRUMB", route.meta.breadcrumb);
});
