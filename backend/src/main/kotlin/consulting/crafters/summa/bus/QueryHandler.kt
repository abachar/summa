package consulting.crafters.summa.bus

import java.lang.annotation.Inherited
import kotlin.annotation.AnnotationTarget.FUNCTION

@Inherited
@Retention
@Target(FUNCTION)
annotation class QueryHandler