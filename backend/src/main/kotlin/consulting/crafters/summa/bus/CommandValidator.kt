package consulting.crafters.summa.bus

import arrow.core.Try

interface CommandValidator<T> {
    fun validate(command: T): Validated
}
