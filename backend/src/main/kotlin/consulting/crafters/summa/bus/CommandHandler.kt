package consulting.crafters.summa.bus

import java.lang.annotation.Inherited
import kotlin.annotation.AnnotationTarget.FUNCTION
import kotlin.reflect.KClass

@Inherited
@Retention
@Target(FUNCTION)
annotation class CommandHandler(
        val validator: KClass<out CommandValidator<*>>
)