package consulting.crafters.summa.bus

interface CommandBus {
    fun <R, C : Command<R>> dispatch(command: C): R
}