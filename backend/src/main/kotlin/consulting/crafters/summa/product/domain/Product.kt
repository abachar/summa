package consulting.crafters.summa.product.domain

import java.util.UUID.randomUUID

data class Product(
        val name: String,
        val description: String,
        val amount: Int,
        val unit: Unit,
        val vatRate: Int,
        val id: String = randomUUID().toString()
)