package consulting.crafters.summa.product.domain

data class CreateProductCommand(
        val name: String,
        val description: String,
        val amount: Int,
        val unit: Unit,
        val vatRate: Int
) {
    fun toProduct(): Product {
        return Product(name, description, amount, unit, vatRate)
    }
}

/*
class CreateProductCommandValidator : CommandValidator<CreateProductCommand> {
    override fun validate(command: CreateProductCommand): Validated<Product, ValidationErrors> {
    }
}

open class CreateProductHandler(val productRepository: ProductRepository) {

    @CommandHandler(validator = CreateProductCommandValidator::class)
    fun handle(command: CreateProductCommand): Try<String> {
        val product = command.toProduct()
        return Try {
            productRepository.save(product)
            product.id
        }
    }
}
        */