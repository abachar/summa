package consulting.crafters.summa.product.domain

interface ProductRepository {
    fun save(product: Product)
}