package consulting.crafters.summa.client.infrastructure.api

import consulting.crafters.summa.bus.CommandBus
import consulting.crafters.summa.client.domain.Client
import consulting.crafters.summa.client.domain.DisableClientCommand
import consulting.crafters.summa.client.domain.FindClientsQuery
import consulting.crafters.summa.client.domain.FindOneClientQuery
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.http.ResponseEntity.*
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/v1/clients")
class ClientController(private val commandBus: CommandBus) {

    @GetMapping
    fun findAll(@RequestParam query: String): List<Client> {
        return commandBus.handle(FindClientsQuery(query))
    }

    @GetMapping("/{clientId}")
    fun findOne(@PathVariable clientId: String): ResponseEntity<Client> {
        return commandBus.handle(FindOneClientQuery(clientId))
                .fold({ notFound().build() }, { ok(it) })
    }

    @PostMapping
    fun create(@RequestBody request: CreateClientRequest): ResponseEntity<*> {
        return commandBus.handle(request.toCommand())
                .fold({ badRequest().body(it) }, { status(HttpStatus.CREATED).body(it) })
    }

    @PutMapping("/{clientId}")
    fun edit(@RequestBody request: EditClientRequest): ResponseEntity<*> {
        return commandBus.handle(request.toCommand())
                .fold({ badRequest().body(it) }, { ok().build() })
    }

    @DeleteMapping("/{clientId}")
    fun disable(@PathVariable clientId: String): ResponseEntity<Void> {
        return commandBus.handle(DisableClientCommand(clientId))
                .fold({ notFound().build() }, { noContent().build() })

    }
}