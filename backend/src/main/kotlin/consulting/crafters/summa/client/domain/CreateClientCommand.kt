package consulting.crafters.summa.client.domain

// import arrow.core.Try
import consulting.crafters.summa.bus.Command

data class CreateClientCommand(
        val name: String,
        val vatNumber: String,
        val contact: Contact,
        val address: Address
) // : Command<Try<String>>