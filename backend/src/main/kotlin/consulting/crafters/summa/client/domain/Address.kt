package consulting.crafters.summa.client.domain

data class Address(
        val street: String,
        val zipCode: String,
        val city: String,
        val country: String
)
