package consulting.crafters.summa.client.infrastructure.mongo

import consulting.crafters.summa.client.domain.Client
import org.springframework.data.repository.reactive.ReactiveCrudRepository
import org.springframework.stereotype.Repository

@Repository
interface ClientReactiveMongoRepository : ReactiveCrudRepository<Client, String>