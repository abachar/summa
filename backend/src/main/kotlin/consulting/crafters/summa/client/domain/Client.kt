package consulting.crafters.summa.client.domain

data class Client(
        val id: String,
        val name: String,
        val vatNumber: String,
        val contact: Contact,
        val address: Address
)