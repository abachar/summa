package consulting.crafters.summa.client.domain

import consulting.crafters.summa.bus.Command

data class FindClientsQuery(val query: String) : Command<List<Client>>