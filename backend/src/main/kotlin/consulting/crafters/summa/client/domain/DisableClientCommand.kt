package consulting.crafters.summa.client.domain

// import arrow.core.Try
import consulting.crafters.summa.bus.Command

data class DisableClientCommand(val clientId: String) // : Command<Try<Void>>