package consulting.crafters.summa.client.domain

data class Contact(
        val name: String, // Mr. Anderson
        val email: String,
        val phone: String
)