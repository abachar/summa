package consulting.crafters.summa.client.infrastructure.api

import arrow.core.Option.Companion.empty
import consulting.crafters.summa.client.domain.ClientService
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@ExtendWith(SpringExtension::class)
@WebMvcTest(ClientController::class)
class ClientControllerTest {
    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var clientService: ClientService

    @Test
    fun `should return 404 when client does not exist`() {
        // Given
        every { clientService.findOne(any()) } returns empty()
        // When
        val result = mockMvc.perform(get("/v1/clients/unknown-user"))
        // Then
        result.andExpect(status().isNotFound)
    }


    @TestConfiguration
    class ClientControllerTestConfig {
        @Bean
        fun clientService() = mockk<ClientService>()
    }
}